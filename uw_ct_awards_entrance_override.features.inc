<?php
/**
 * @file
 * uw_ct_awards_entrance_override.features.inc
 */

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_ct_awards_entrance_override_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_undergraduate_award-field_undergard_award_term'])) {
    $data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['label'] = 'hidden'; /* WAS: 'above' */
    $data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['type'] = 'hidden'; /* WAS: 'taxonomy_term_reference_csv' */
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['module']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['element_class']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['element_option']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['links_option']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['separator_option']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['wrapper_class']);
    unset($data['node-uw_undergraduate_award-field_undergard_award_term']['display']['default']['settings']['wrapper_option']);
  }
  if (isset($data['node-uw_undergraduate_award-field_undergrad_award_aff'])) {
    $data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['label'] = 'hidden'; /* WAS: 'inline' */
    $data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['type'] = 'hidden'; /* WAS: 'taxonomy_term_reference_csv' */
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['module']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['element_class']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['element_option']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['links_option']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['separator_option']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['wrapper_class']);
    unset($data['node-uw_undergraduate_award-field_undergrad_award_aff']['display']['default']['settings']['wrapper_option']);
  }
  if (isset($data['node-uw_undergraduate_award-field_undergrad_award_contact'])) {
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['display']['default']['label'] = 'hidden'; /* WAS: 'above' */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['display']['default']['type'] = 'hidden'; /* WAS: 'text_default' */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['required'] = 0; /* WAS: 1 */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['settings']['better_formats']['allowed_formats']['single_page_remote_events'] = 0; /* WAS: '' */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['settings']['better_formats']['allowed_formats']['uw_tf_conference'] = 0; /* WAS: '' */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['settings']['better_formats']['default_order_wrapper']['formats']['single_page_remote_events'] = array(
      'weight' => 0,
    ); /* WAS: '' */
    $data['node-uw_undergraduate_award-field_undergrad_award_contact']['settings']['better_formats']['default_order_wrapper']['formats']['uw_tf_conference'] = array(
      'weight' => 0,
    ); /* WAS: '' */
    unset($data['node-uw_undergraduate_award-field_undergrad_award_contact']['display']['default']['module']);
  }
  if (isset($data['node-uw_undergraduate_award-field_undergrad_award_ptype'])) {
    $data['node-uw_undergraduate_award-field_undergrad_award_ptype']['display']['default']['settings']['links_option'] = 0; /* WAS: FALSE */
    $data['node-uw_undergraduate_award-field_undergrad_award_ptype']['label'] = 'Application required?'; /* WAS: 'Selection process' */
  }
  if (isset($data['node-uw_undergraduate_award-field_undregrad_award_enroll'])) {
    $data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['label'] = 'hidden'; /* WAS: 'inline' */
    $data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['type'] = 'hidden'; /* WAS: 'taxonomy_term_reference_csv' */
    $data['node-uw_undergraduate_award-field_undregrad_award_enroll']['required'] = 0; /* WAS: 1 */
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['module']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['element_class']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['element_option']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['links_option']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['separator_option']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['wrapper_class']);
    unset($data['node-uw_undergraduate_award-field_undregrad_award_enroll']['display']['default']['settings']['wrapper_option']);
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_awards_entrance_override_strongarm_alter(&$data) {
  if (isset($data['field_bundle_settings_node__uw_undergraduate_award'])) {
    $data['field_bundle_settings_node__uw_undergraduate_award']->value['view_modes']['embedded'] = array(
      'custom_settings' => TRUE,
    ); /* WAS: '' */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_ct_awards_entrance_override_views_default_views_alter(&$data) {
  if (isset($data['uw_undergrad_award_search_block'])) {
    $data['uw_undergrad_award_search_block']->display['page']->display_options['filters']['field_undergrad_award_ptype_tid']['expose']['label'] = 'Application required?'; /* WAS: 'Selection process:' */
    $data['uw_undergrad_award_search_block']->display['page']->display_options['filters']['field_undergrad_award_ptype_tid']['expose']['remember_roles'][20] = 0; /* WAS: '' */
    $data['uw_undergrad_award_search_block']->display['page']->display_options['filters']['field_undergrad_award_ptype_tid']['expose']['remember_roles'][21] = 0; /* WAS: '' */
    $data['uw_undergrad_award_search_block']->display['page']->display_options['filters']['field_undergrad_award_ptype_tid']['expose']['remember_roles'][23] = 0; /* WAS: '' */
  }
}
