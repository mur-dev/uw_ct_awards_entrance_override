<?php
/**
 * @file
 * uw_ct_awards_entrance_override.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_awards_entrance_override_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|label"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|module"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|element_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|element_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|links_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|separator_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|wrapper_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|settings|wrapper_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergard_award_term.display|default|type"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|label"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|module"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|element_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|element_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|links_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|separator_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|wrapper_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|settings|wrapper_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_aff.display|default|type"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.display|default|label"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.display|default|module"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.display|default|type"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.required"] = 0;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.settings|better_formats|allowed_formats|single_page_remote_events"] = 0;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.settings|better_formats|allowed_formats|uw_tf_conference"] = 0;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.settings|better_formats|default_order_wrapper|formats|single_page_remote_events"] = array(
    'weight' => 0,
  );
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_contact.settings|better_formats|default_order_wrapper|formats|uw_tf_conference"] = array(
    'weight' => 0,
  );
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_ptype.display|default|settings|links_option"] = 0;
  $overrides["field_instance.node-uw_undergraduate_award-field_undergrad_award_ptype.label"] = 'Application required?';
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|label"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|module"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|element_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|element_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|links_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|separator_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|wrapper_class"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|settings|wrapper_option"]["DELETED"] = TRUE;
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.display|default|type"] = 'hidden';
  $overrides["field_instance.node-uw_undergraduate_award-field_undregrad_award_enroll.required"] = 0;

  // Exported overrides for: variable
  $overrides["variable.field_bundle_settings_node__uw_undergraduate_award.value|view_modes|embedded"] = array(
    'custom_settings' => TRUE,
  );

  // Exported overrides for: views_view
  $overrides["views_view.uw_undergrad_award_search_block.display|page|display_options|filters|field_undergrad_award_ptype_tid|expose|label"] = 'Application required?';
  $overrides["views_view.uw_undergrad_award_search_block.display|page|display_options|filters|field_undergrad_award_ptype_tid|expose|remember_roles|20"] = 0;
  $overrides["views_view.uw_undergrad_award_search_block.display|page|display_options|filters|field_undergrad_award_ptype_tid|expose|remember_roles|21"] = 0;
  $overrides["views_view.uw_undergrad_award_search_block.display|page|display_options|filters|field_undergrad_award_ptype_tid|expose|remember_roles|23"] = 0;

 return $overrides;
}
